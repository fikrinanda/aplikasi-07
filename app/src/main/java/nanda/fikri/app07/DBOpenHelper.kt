package nanda.fikri.app07

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(contet : Context): SQLiteOpenHelper(contet, DB_Name,null, DB_VER) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tMhs = "create table mhs(nim text not null, nama text not null, prodi text not null)"
        db?.execSQL(tMhs)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object{
        val DB_Name = "mahasiswa"
        val DB_VER = 1
    }

}