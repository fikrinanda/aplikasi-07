package nanda.fikri.app07

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import android.widget.Toast
import com.google.zxing.BarcodeFormat
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var db : SQLiteDatabase
    lateinit var lsAdapter : ListAdapter

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnScanQR -> {
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
            R.id.btnGenerateQR ->{
                val barCodeEncode = BarcodeEncoder()
                val bitmap = barCodeEncode.encodeBitmap(edQrCode.text.toString(),
                    BarcodeFormat.QR_CODE,500,500)
                imV.setImageBitmap(bitmap)
            }
            R.id.btnSimpan ->{
                insertDataMhs(txNIM.text.toString(), txNama.text.toString(), txProdi.text.toString())

            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val intentResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data)

        if(intentResult!=null){
            if(intentResult.contents != null){
                edQrCode.setText(intentResult.contents)
                val strToken = StringTokenizer(edQrCode.text.toString(), ";", false)
                txNIM.setText(strToken.nextToken())
                txNama.setText(strToken.nextToken())
                txProdi.setText(strToken.nextToken())
            }
            else{
                Toast.makeText(this, "Dibatalkan",Toast.LENGTH_SHORT).show()

            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    lateinit var intentIntegrator: IntentIntegrator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        db = DBOpenHelper(this).writableDatabase
        intentIntegrator = IntentIntegrator(this)
        btnGenerateQR.setOnClickListener(this)
        btnScanQR.setOnClickListener(this)
        btnSimpan.setOnClickListener(this)



    }

    fun insertDataMhs(nim : String, nama : String, prodi : String){
        var sql = "insert into mhs(nim, nama, prodi) values (?,?,?)"
        db.execSQL(sql, arrayOf(nim, nama, prodi))
        txNIM.setText("")
        txNama.setText("")
        txProdi.setText("")
        showDataMhs()
    }

    fun showDataMhs(){
        val cursor : Cursor = db.query("mhs", arrayOf("nim as _id","nama","prodi"),
            null,null,null,null,"nim asc")
        lsAdapter = SimpleCursorAdapter(this,R.layout.item_data_mhs,cursor,
            arrayOf("_id","nama","prodi"), intArrayOf(R.id.txNIM, R.id.txNama, R.id.txNamaProdi),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lv.adapter = lsAdapter
    }


    override fun onStart() {
        super.onStart()
        showDataMhs()
    }
}
